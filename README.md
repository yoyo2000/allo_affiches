## Obsolète : 

allociné.fr refuse désormais les connections par script.
Passez à une version de ce projet qui s'appuie sur themoviedb.org : 
https://framagit.org/yoyo2000/tmdb_affiches


## Description

Scrute un dossier et génère un mur d'affiches de cinéma (page html) avec les fichiers de film trouvés (s'appuie sur une recherche allociné).

**Usage :**
```
python3 allo_affiches.py -h
usage: allo_affiches.py [-h] [-r] [-i] [-d | -n] [dossier_films]

Génère un mur d'affiches de cinéma à partir d'un dossier de films et du site
allocine.fr

positional arguments:
  dossier_films     chemin vers le dossier contenant les films à scanner
                    (dossier courant par défaut)

optional arguments:
  -h, --help        show this help message and exit
  -r, --recursive   scanne le dossier de manière récursive
  -i, --interactif  demande la référence allocine si aucune fiche n'est
                    trouvée
  -d, --date        trie les résultats par date au lieu de l'ordre
                    alphabétique
  -n, --note        trie les résultats par note au lieu de l'ordre
                    alphabétique
  -w, --windows     génère un script Windows permettant de relancer la
                    commande ultérieurement
  -l, --linux       génère un script Linux permettant de relancer la commande
                    ultérieurement
```

Crée un dossier **allo_affiches** dans le dossier des vidéos contenant la page html générée `index.html`, les images récupérées depuis allociné `dossier images` et un fichier de références `allo_affiches.json` permettant de ne pas re-scanner les films déjà pris en compte.

Pour être trouvés, les fichiers doivent avoir leur nom correspondant au titre du film. Le premier résultat de la recherche allociné est choisi d'office. En mode intéractif, si la recherche ne donne aucun résultat, la référence allociné est demandée.
Dans le mur d'affiches, le survol donne des informations sur le film, le clic mène à la fiche du film sur allociné.

Si le premier résultat d'allociné n'est pas le bon pour certains films, un autre script permet de rectifier les erreurs en proposant de choisir parmis les résultats de la recherche allociné.

```shell
python3 correction_affiches.py "/chemin/vers/dossier/films" "référence du film1" ["référence du film2" ...]
```

Les références de film correspondent à l'id de l'affiche dans le fichier html ainsi qu'au nom du fichier image sans l'extension.
Lors de la question "_Entrer le code allociné choisi_", il faut saisir un des numéros inscrits devant chaque résultat de recherche (c'est le code allociné du film). On pourra aussi saisir un autre code allociné même s'il n'est pas proposé.

## Installation

La bibliothèque [Beautiful Soup 4](https://www.crummy.com/software/BeautifulSoup) est nécessaire.

Pour l'installer sous Debian/Ubuntu :
```shell
sudo apt install python3-bs4
```

Le moteur de templating [Jinja2](http://jinja.pocoo.org/) est utilisé avec le fichier `allo_affiches.html`.

Pour l'installer sous Debian/Ubuntu :
```shell
sudo apt install python3-jinja2
```

Testé sur Ubuntu et Windows.

## License

WTFPL : http://www.wtfpl.net/
