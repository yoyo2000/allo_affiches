#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
import argparse
import glob
import sys
import time
import datetime
import re
import unicodedata
import html
import json
import webbrowser
from urllib.request import urlopen
from urllib.parse import quote
from urllib.error import HTTPError
import bs4
from jinja2 import Environment, FileSystemLoader
from requests_html import HTMLSession

EXTENSIONS = ["avi", "mp4", "mpeg", "divx", "mkv", "flv"]
URL_ALLOCINE = "http://www.allocine.fr"
URL_ALLOCINE_FICHE = URL_ALLOCINE + "/film/fichefilm_gen_cfilm={0}.html"
SEARCH = "/recherche/movie/?q={0}"
NOM_DOSSIER_ALLO = "allo_affiches"
NOM_DOSSIER_IMG = "images"
EXT_IMG = ".jpg"    # Force cette extension aux fichiers image téléchargés

def slugify(value):
    """
    Normalizes string, converts to lowercase, removes non-alpha characters,
    and converts spaces to hyphens.
    """
    value = unicodedata.normalize('NFKD', value).encode('ascii', 'ignore')
    value = re.sub(r'[^\w\s-]', '', value.decode('utf-8')).strip()
    return value


def verif_dossier(nom_dossier, fatal=True):
    """Vérifie si un dossier existe et si non, sort si fatal est à True"""
    if not os.path.isdir(nom_dossier):
        print("Erreur : Le dossier {0} n'existe pas.".format(nom_dossier))
        if fatal:
            sys.exit()

def recup_info_film(url):
    """Va chercher les infos d'un film à l'adresse donnée (fiche allocine)"""
    fiche = {}
    filmfile = urlopen(url).read()
    filmpage = bs4.BeautifulSoup(filmfile, "lxml")

    fiche['url'] = url
    tit = filmpage.find("title").get_text().split(" - ")
    fiche['titre'] = tit[0]
    fiche['annee'] = tit[-2].split(" ")[1]

    gen = filmpage.findAll("div", {"class" : "meta-body-item meta-body-info"})[0].get_text().strip()
    value = re.sub(r'  ', '', gen)
    value = value.split("\n")
    value = ''.join(value).split("/")

    try:
        fiche['duree'] = value[1]
    except(IndexError, AttributeError):
        fiche['duree'] = "PAS DE DURÉE"
    try:
        fiche['genre'] = value[2].replace(",",", ")
    except(IndexError, AttributeError):
        fiche['genre'] = "PAS DE GENRE"
    syn = filmpage.find("section", {"id" : "synopsis-details"}).find("div", {"class" : "content-txt"})
    fiche['synopsis'] = html.escape(syn.get_text().strip()) if syn is not None else "PAS DE SYNOPSIS"
    fiche['url_image'] = filmpage.find("meta", {"property" : "og:image"}).get("content")
    try:
        note = filmpage.find("div", {"class" : "rating-holder"}).findAll("span", {"class":"stareval-note"})[-2]
        fiche['note'] = note.get_text() + "/5" if note is not None else "PAS DE NOTE"
    except(IndexError, AttributeError):
        fiche['note'] = "PAS DE NOTE"

    return fiche

def gen_script(args, chemin_allo_affiches, chemin_destination):
    """
        Génère les scripts pour relancer la commande ultérieument en se basant sur les templates allo_affiches.bat et allo_affiches.sh

        :param args: options de ligne de commande
        :param chemin_allo_affiches: chemin vers le script en cours d'exécution
        :param chemin_destination: chemin où écrire les scripts générés (dossier allo_affiches dans le dossier de vidéos)
        :type args: dict
        :type chemin_allo_affiches: str
        :type chemin_destination: str
    """
    if not (args.windows or args.linux): return   # Pas de génération de script demandée

    # Récup des chemins et des options
    commande_allo_affiches = os.path.basename(__file__)
    if args.windows:
        lettre_lecteur = chemin_allo_affiches[:2]

    chemin_videos = args.dossier_films
    options_allo_affiches = ""
    options_allo_affiches += "-r " if args.recursive else ""
    options_allo_affiches += "-d " if args.date else ""
    options_allo_affiches += "-n " if args.note else ""

    # Génération des scripts
    j2_env = Environment(loader=FileSystemLoader(chemin_allo_affiches), trim_blocks=True)
    if args.windows:
        with open(os.path.join(chemin_destination, "allo_affiches.bat"), "wb") as fich:
            fich.write(j2_env.get_template('allo_affiches.bat').render(
                lettre_lecteur=lettre_lecteur,
                chemin_allo_affiches=chemin_allo_affiches,
                commande_allo_affiches=commande_allo_affiches,
                options_allo_affiches=options_allo_affiches,
                chemin_videos=chemin_videos
            ).encode("utf-8"))
            fich.flush()

    if args.linux:
        with open(os.path.join(chemin_destination, "allo_affiches.sh"), "wb") as fich:
            fich.write(j2_env.get_template('allo_affiches.sh').render(
                chemin_allo_affiches=chemin_allo_affiches,
                commande_allo_affiches=f"python3 {commande_allo_affiches}",
                options_allo_affiches=options_allo_affiches,
                chemin_videos=chemin_videos
            ).encode("utf-8"))
            fich.flush()

def main():
    """Appel principal"""
    # Récupérer les arguments de ligne de commande
    parser = argparse.ArgumentParser(description="Génère un mur d'affiches de cinéma à partir d'un dossier de films et du site allocine.fr", epilog="Enjoy ;)")
    parser.add_argument("-r", "--recursive", action="store_true", help="scanne le dossier de manière récursive")
    parser.add_argument("-i", "--interactif", action="store_true", help="demande la référence allocine si aucune fiche n'est trouvée")
    group = parser.add_mutually_exclusive_group()
    group.add_argument("-d", "--date", action="store_true", help="trie les résultats par date au lieu de l'ordre alphabétique")
    group.add_argument("-n", "--note", action="store_true", help="trie les résultats par note au lieu de l'ordre alphabétique")
    parser.add_argument("-w", "--windows", action="store_true", help="génère un script Windows permettant de relancer la commande ultérieurement")
    parser.add_argument("-l", "--linux", action="store_true", help="génère un script Linux permettant de relancer la commande ultérieurement")
    parser.add_argument("-s", "--save", action="store_true", help="sauve les fiches une par une")
    parser.add_argument("dossier_films", nargs='?', help="chemin vers le dossier contenant les films à scanner (dossier courant par défaut)", default=os.getcwd())
    args = parser.parse_args()
    dossier_films = args.dossier_films

    verif_dossier(dossier_films)

    # Scruter le dossier de video et en déduire une liste de fichier à traiter
    if args.recursive:
        liste_fichier = glob.glob(os.path.join(dossier_films, "**", "*"), recursive=True)   # Récursif
    else:
        liste_fichier = glob.glob(os.path.join(dossier_films, "*"))                         # Non récursif

    titres = []
    dates = []
    for element in liste_fichier:
        if not os.path.isdir(element):
            if os.path.splitext(element)[1][-3:] in EXTENSIONS:
                titres.append(re.sub(r"[_.]", " ", os.path.splitext(os.path.basename(element))[0]))
                tms = os.path.getmtime(element)
                dte = datetime.datetime.fromtimestamp(tms).strftime('%Y-%m-%d %H:%M:%S')
                dates.append(dte)

    # Créer les dossiers de résultat
    if titres:
        dossier_allo = os.path.join(dossier_films, NOM_DOSSIER_ALLO)
        if not os.path.isdir(dossier_allo):
            os.mkdir(dossier_allo)
        dossier_img = os.path.join(dossier_allo, NOM_DOSSIER_IMG)
        if not os.path.isdir(dossier_img):
            os.mkdir(dossier_img)
    else:
        print("Erreur : Aucun film trouvé dans {0}.".format(dossier_films))
        sys.exit()

    # Récupérer un éventuel précédent scan
    json_file = os.path.join(dossier_allo, "allo_affiches.json")
    titres_vus = []
    try:
        with open(json_file, "r", encoding="utf-8") as fich:
            films_vus = json.load(fich)
        titres_vus = [vu["key"] for vu in films_vus]
    except:
        pass

    # Pour chaque film, trouver la fiche allociné et récupérer les infos et image d'affiche
    total = len(titres)
    print("{0} films trouvés :".format(total))
    films = []
    erreurs = []
    for index, titre in enumerate(titres):
        print("#{0}/{1}: {2}".format(index+1, total, titre))
        titre_slug = slugify(titre)
        if titre_slug in titres_vus:
            # Film déjà scanné
            print("\t\tRéférence : {0}".format(titre_slug))
            film = list(filter(lambda x: titre_slug == x["key"], films_vus))[0]
            #film['filename'] = titre
            print("\t\tFilm déjà scanné : {0}".format(film['url']))
        else:
            # Recherche du film
            print("\t\tRéférence : {0}".format(titre_slug))
            url_film = ""
            try:
                
                url_search = URL_ALLOCINE + SEARCH.format(quote(titre))
                session = HTMLSession()
                searchfile = session.get(url_search, timeout=None)
                searchfile.html.render(timeout=0)
                
                print("\t\tRecherche : {0}".format(url_search))
                #searchfile = urlopen(url_search).read()
            except HTTPError:
                if args.interactif :
                    reponse = input("\tEntrer le code allociné choisi pour {0} : ".format(titre))
                    url_film = URL_ALLOCINE_FICHE.format(reponse)
                else:
                    msg = "\t\tErreur : Recherche impossible dans {0}.".format(titre)
                    print(msg)
                    erreurs.append(msg)
                    continue

            try:
                if url_film == "":
                    #searchpage = bs4.BeautifulSoup(searchfile, "lxml")
                    l = list(searchfile.html.find("li.mdl", first=True).links)
                    href_fiche = list(filter(lambda x: "fichefilm_gen_cfilm" in x, l))[0]
            except(HTTPError, AttributeError):
                if args.interactif :
                    reponse = input("\tEntrer le code allociné choisi pour {0} : ".format(titre))
                    url_film = URL_ALLOCINE_FICHE.format(reponse)
                else:
                    msg = "\t\tErreur : Fiche introuvable pour {0}.".format(titre)
                    print(msg)
                    erreurs.append(msg)
                    continue
            else:
                if url_film == "":
                    url_film = URL_ALLOCINE + str(href_fiche)

            try:
                print("\t\tFiche film : {0}".format(url_film))
                fiche_film = recup_info_film(url_film)
            except(HTTPError, AttributeError):
                msg = "\t\tErreur : Fiche introuvable pour {0}.".format(titre)
                print(msg)
                erreurs.append(msg)
                continue

            film = {}
            film['key'] = titre_slug
            film['filename'] = titre
            film['date'] = dates[index]
            film.update(fiche_film)

            try:
                raw_img = urlopen(fiche_film['url_image']).read()
            except HTTPError:
                msg = "\t\tErreur : Image introuvable pour {0}.".format(titre)
                print(msg)
                erreurs.append(msg)
                continue

            img_file = os.path.join(dossier_img, film['key'] + EXT_IMG)
            if os.path.isfile(img_file):
                os.remove(img_file)
            with open(img_file, "wb") as fich:
                fich.write(raw_img)

            if args.save:
                if os.path.isfile(json_file):
                    os.replace(json_file, os.path.join(dossier_allo, "allo_affiches.back.json"))     # petit backup
                with open(json_file, "w", encoding="utf-8") as fich:
                    json.dump(films, fich, ensure_ascii=False, indent=2)

        films.append(film)

    # Suppression des images correspondant à des films supprimés
    imgs_ok = [r["key"] for r in films] # Images de films venant d'être ajoutés
    imgs_a_verif = [i[:-4] for i in os.listdir(dossier_img) if not os.path.isdir(os.path.join(dossier_img, i))] # Fichiers image trouvés dans le dossier d'images
    imgs_a_suppr = [i for i in imgs_a_verif if i not in imgs_ok] # Images à supprimer (présentes dans imgs_a_verif mais pas dans imgs_ok)
    for i in imgs_a_suppr:
        img_file = os.path.join(dossier_img, i + EXT_IMG)
        if os.path.isfile(img_file):
            os.remove(img_file)

    # Générer la page html des affiches et le fichier de référence
    if films:
        if args.date:
            films = sorted(films, key=lambda x: x['date'])          # Classement des films par date d'ajout dans le dossier de vidéos
        elif args.note:
            films = sorted(films, key=lambda x: x['note'] if x['note'] != "PAS DE NOTE" else "0", reverse=True) # Classement des films par notes décroissantes
        else:
            films = sorted(films, key=lambda x: x['key'].lower())   # Classement des films par ordre alphabétique des références

        if os.path.isfile(json_file):
            os.replace(json_file, os.path.join(dossier_allo, "allo_affiches.back.json"))     # petit backup
        with open(json_file, "w", encoding="utf-8") as fich:
            json.dump(films, fich, ensure_ascii=False, indent=2)

        cur_dir = os.path.dirname(os.path.abspath(__file__))
        j2_env = Environment(loader=FileSystemLoader(cur_dir), trim_blocks=True)
        with open(os.path.join(dossier_allo, "index.html"), "wb") as fich:
            fich.write(j2_env.get_template('allo_affiches.html').render(films=films, EXT_IMG=EXT_IMG).encode("utf-8"))
            fich.flush()
            time.sleep(3)
            webbrowser.open(fich.name, new=2)

    # Génération du ou des scripts pour relancer la commande ultérieurement
    gen_script(args, cur_dir, dossier_allo)

    # Récapitulatif des erreurs
    if erreurs:
        print("\n")
        print("#"*50)
        print("Des erreurs se sont produites :")
        for err in erreurs:
            print(err)
        print("#"*50)

if __name__ == '__main__':
    main()
