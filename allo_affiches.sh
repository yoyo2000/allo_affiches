#!/usr/bin/env bash

# Changement de dossier
cd {{ chemin_allo_affiches }}

# Appel allo_affiches avec options
{{ commande_allo_affiches }} {{ options_allo_affiches }} {{ chemin_videos }}

# Pause pour voir les erreurs éventuelles
read -s -n1 -p "Appuyez sur une touche pour continuer..."; echo