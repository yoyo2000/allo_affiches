REM Changement de lecteur
{{ lettre_lecteur }}

REM Changement de dossier
cd {{ chemin_allo_affiches }}

REM Appel allo_affiches avec options
{{ commande_allo_affiches }} {{ options_allo_affiches }} {{ chemin_videos }}

REM Pause pour voir les erreurs éventuelles
pause
cmd