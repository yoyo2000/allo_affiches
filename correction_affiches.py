#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
import sys
import re
import json
import webbrowser
from urllib.request import urlopen
from urllib.parse import quote
from urllib.error import HTTPError
import bs4
from jinja2 import Environment, FileSystemLoader

import allo_affiches as aa

def main():
    """Appel principal"""
    # Récupérer le dossier de travail et les films à traiter dans les arguments
    if len(sys.argv) <= 2:
        print("""Usage : correction_affiches.py "/chemin/vers/dossier/films" "référence du film1" ["référence du film2" ...]

        "chemin/vers/films" => Chemin vers le dossier contenant les vidéos
        "référence du film" => Référence du film à corriger (correspond à l'id de l'affiche et au nom du fichier image sans l'extension)
        """)
        sys.exit()

    dossier_films = sys.argv[1]
    keys_arg = sys.argv[2:]

    dossier_allo = os.path.join(dossier_films, aa.NOM_DOSSIER_ALLO)
    aa.verif_dossier(dossier_allo)
    dossier_img = os.path.join(dossier_allo, aa.NOM_DOSSIER_IMG)
    aa.verif_dossier(dossier_img)

    # Charger le fichier json
    json_file = os.path.join(dossier_allo, "allo_affiches.json")
    try:
        with open(json_file, "r", encoding="utf-8") as fich:
            films_vus = json.load(fich)
        titres_vus = {vu["key"]:vu["filename"] for vu in films_vus}
    except:
        print("Erreur : Problème pour lire le fichier {0}.".format(json_file))
        sys.exit()

    # Traitement de la liste de film à corriger
    for key_arg in keys_arg:
        if key_arg not in titres_vus:
            print("Erreur : Référence '{0}' non trouvée.".format(key_arg))
            continue

        # Recherche du film
        try:
            titre_a_chercher = titres_vus[key_arg]
            url_search = aa.URL_ALLOCINE + aa.SEARCH.format(quote(titre_a_chercher))
            print("\nRecherche pour '{0}' : {1}".format(titre_a_chercher, url_search))
            searchfile = urlopen(url_search).read()
        except HTTPError:
            print("\tErreur : Recherche impossible dans {0}.".format(url_search))
            continue

        try:
            searchpage = bs4.BeautifulSoup(searchfile, "lxml")
            fiche = searchpage.find("table", {"class" : "totalwidth noborder purehtml"}).find_all(href=re.compile("fichefilm_gen_cfilm"))
            for i, choix in enumerate(fiche):
                if i % 2:   # les liens sont doublés (un pour l'affiche et un pour les infos) on ne prend que le 2ème
                    ref = "{0:>10s}".format(choix.get("href").split("=")[1][:-5])
                    print("\n\t", ref, "\t===>\t", choix.get_text(" ", strip=True) + " (" + choix.find_next("span").get_text(" ", strip=True) + ")")
                    print("\t", "{0:>10s}".format(" "), "\t===>\t", aa.URL_ALLOCINE + choix.get("href"))

            reponse = input("\tEntrer le code allociné choisi : ")
            url_fiche = aa.URL_ALLOCINE_FICHE.format(reponse)
            print("\tFiche film : {0}".format(url_fiche))
            fiche_film = aa.recup_info_film(url_fiche)
        except(HTTPError, AttributeError):
            print("\t\tErreur : Fiche introuvable pour {0}.".format(key_arg))
            continue

        indice = films_vus.index(list(filter(lambda x: key_arg == x["key"], films_vus))[0])
        films_vus[indice].update(fiche_film)

        try:
            raw_img = urlopen(fiche_film['url_image']).read()
        except HTTPError:
            print("\t\tErreur : Image introuvable pour {0}.".format(key_arg))
            continue

        img_file = os.path.join(dossier_img, films_vus[indice]['key'] + aa.EXT_IMG)
        if os.path.isfile(img_file):
            os.remove(img_file)
        with open(img_file, "wb") as fich:
            fich.write(raw_img)

    # Générer la page html des affiches et le fichier de référence
    if os.path.isfile(json_file):
        os.replace(json_file, os.path.join(dossier_allo, "allo_affiches.back.json"))     # petit backup
    with open(json_file, "w", encoding="utf-8") as fich:
        json.dump(films_vus, fich, ensure_ascii=False, indent=2)

    cur_dir = os.getcwd()
    j2_env = Environment(loader=FileSystemLoader(cur_dir), trim_blocks=True)
    with open(os.path.join(dossier_allo, "index.html"), "wb") as fich:
        fich.write(j2_env.get_template('allo_affiches.html').render(films=films_vus, EXT_IMG=aa.EXT_IMG).encode("utf-8"))
        fich.flush()
        webbrowser.open(fich.name, new=2)

if __name__ == '__main__':
    main()
