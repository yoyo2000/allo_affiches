#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''Juste une moulinette qui reprend un fichier json de films et ajoute/met à jour le titre, l'année, la note
   Exécuter allo_affiche après pour générer la page html
'''
import json, bs4, re
from urllib.request import urlopen
from os import replace, path, getcwd

json_file = "allo_affiches.json"

with open(json_file, "r", encoding="utf-8") as fich:
    films = json.load(fich)

for i in range(len(films)):
    f = films[i]
    print()
    print(f['titre'])

    if "allocine" not in f['url']:
        continue
    filmfile = urlopen(f['url']).read()
    filmpage = bs4.BeautifulSoup(filmfile, "lxml")

    tit = filmpage.find("title").get_text().split(" - ")
    f['titre'] = tit[0]
    f['annee'] = tit[-2].split(" ")[1]

    gen = filmpage.findAll("div", {"class" : "meta-body-item meta-body-info"})[0].get_text().strip()
    value = re.sub(r'  ', '', gen)
    value = value.split("\n")
    value = ''.join(value).split("/")

    try:
        d = f['duree']
        f['duree'] = value[1]
    except(IndexError, AttributeError):
        f['duree'] = "PAS DE DURÉE"
        
    try:
        g = f['genre']
        f['genre'] = value[2].replace(",",", ")
    except(IndexError, AttributeError):
        f['genre'] = "PAS DE GENRE"
        
    try:
        n = f['note']
    except(KeyError):
        f['note'] = "PAS DE NOTE"
    try:
        note = filmpage.find("div", {"class" : "rating-holder"}).findAll("span", {"class":"stareval-note"})[-2]
        f['note'] = note.get_text() + "/5" if note is not None else "PAS DE NOTE"
    except(IndexError, AttributeError):
        f['note'] = "PAS DE NOTE"

    if d != f['duree']:
        print(f"\t{d} => {f['duree']}")
    
    if g != f['genre']:
        print(f"\t{g} => {f['genre']}")
        
    if n != f['note']:
        print(f"\t{n} => {f['note']}")
    films[i].update(f)

replace(json_file, path.join(getcwd(), "allo_affiches.back.json"))     # petit backup
with open(json_file, "w", encoding="utf-8") as fich:
    json.dump(films, fich, ensure_ascii=False, indent=2)
